import Vue from 'vue';
import './plugins/vuetify';
import * as VueGoogleMaps from 'vue2-google-maps';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCNfyz5dpml226DgyQGxDdvJe6ui7bEElU',
    libraries: 'visualization', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    // // If you want to set the version, you can do so:
    // v: '3.26',
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
