import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const ALL_FLIGHTS_API = 'https://opensky-network.org/api/states/all';
const FLIGHT_API = 'https://opensky-network.org/api/tracks/all';

export default new Vuex.Store({
  state: {
    loading: false,
    debounceDelay: 2000,
    position: {
      latitude: 43.66,
      longitude: -79.38,
    },
    bounds: null,
    flights: [],
    selectedFlight: null,
  },
  mutations: {
    flights(state, value) {
      state.flights = value;
    },
    bounds(state, value) {
      state.bounds = value;
    },
    loading(state, value) {
      state.loading = value;
    },
    selectedFlight(state, value) {
      state.selectedFlight = value;
    },
  },
  actions: {
    async retrieveFlights({ commit, state }) {
      const areaMargin = 2;
      commit('loading', true);
      const lamin = state.bounds ? state.bounds.ma.j : state.position.latitude - areaMargin;
      const lomin = state.bounds ? state.bounds.ga.j : state.position.longitude - areaMargin;
      const lamax = state.bounds ? state.bounds.ma.l : state.position.latitude + areaMargin;
      const lomax = state.bounds ? state.bounds.ga.l : state.position.longitude + areaMargin;
      try {
        const resp = await axios.get(ALL_FLIGHTS_API, {
          params: {
            lamin,
            lomin,
            lamax,
            lomax,
          },
        });
        commit('flights', resp.data.states ? resp.data.states : []);
        commit('loading', false);
      } catch (e) {
        commit('loading', false);
        throw e;
      }
    },
    async selectFlight({ commit }, flight) {
      commit('loading', true);
      try {
        const resp = await axios.get(FLIGHT_API, {
          params: {
            time: 0,
            icao24: flight[0],
          },
        });
        commit('selectedFlight', {
          flight,
          tracks: resp.data,
        });
        commit('loading', false);
      } catch (e) {
        commit('loading', false);
        throw e;
      }
    },
  },
});
