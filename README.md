# flight-tracker
A Vue project using the opensky api.

See online demo:
https://flight-tracker.netlify.com

![Screenshot](/flight-tracker.jpeg?raw=true "Screenshot")


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
